defmodule Solution do
  def remove(s), do: s
                     |> String.split("!", trim: true)
                     |> Enum.join()
                     |> Kernel.<>(
                          case Regex.named_captures(~r/^(.*?)(?<postfix>!+)$/, s) do
                            %{"postfix" => postfix} -> postfix
                            _ -> ""
                          end
                        )
end

IO.inspect(Solution.remove(""))
IO.inspect(Solution.remove("Hi"))
IO.inspect(Solution.remove("Hi!"))
IO.inspect(Solution.remove("Hi!!!"))
IO.inspect(Solution.remove("!H!i!!!"))
